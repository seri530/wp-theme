FROM wordpress

RUN mkdir /var/www/html/wp-content/themes/newTemplate
VOLUME [ "/var/www/html/wp-content/themes/newTemplate" ]

WORKDIR /var/www/html/wp-content/themes/newTemplate
