# Wordpress blank theme
This repository helps to create structured WP theme with SASS, Typescript, and Lit-HTML. You can run a separated test environment with Docker

# Setup

## Install tools
1. [Node.js](https://nodejs.org/) Minimum version 6.x, minimum `npm` version 6.x
1. [Docker](https://docker.com/)
1. Install global NPM packages

``
    npm i gulp 
``

## Restore packages
1. Install NPM packages
``
    npm i
``

## Run development environment
1. Start docker environment with

``
 docker-compose up 
``
